package com.rilixtech.library;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R2.id.main_activity_tv)
    TextView mTvMainActivity;
    @BindView(R2.id.text)
    TextView mText;
    @BindView(R2.id.container)
    LinearLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Try to generate Activity injections by clicking to R.layout.activity_main
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new Fragment())
                    .commit();
        }
    }

    @OnTextChanged(R2.id.main_activity_tv)
    public void onTvMainActivityTextChanged(CharSequence text) {
        //TODO: add onTextChanged handling
    }
}

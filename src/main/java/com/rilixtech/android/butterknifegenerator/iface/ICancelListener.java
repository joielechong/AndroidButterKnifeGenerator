package com.rilixtech.android.butterknifegenerator.iface;

public interface ICancelListener {

    void onCancel();
}

package com.rilixtech.android.butterknifegenerator.butterknife;

/**
 * ButterKnife version 9
 *
 * @author Joielechong
 * @since 1.8.0
 */
public class ButterKnife9 extends AbstractButterKnife {

    private static final String mFieldAnnotationSimpleName = "BindView";
    private static final String mSimpleBindStatement = "ButterKnife.bind";
    private static final String mSimpleUnbindStatement = ".unbind";
    private static final String mUnbinderClassSimpleName = "Unbinder";

    @Override
    public String getVersion() {
        return "9.0.0-rc3";
    }

    @Override
    public String getDistinctClassName() {
        return getFieldAnnotationCanonicalName();
    }

    @Override
    public String getFieldAnnotationSimpleName() {
        return mFieldAnnotationSimpleName;
    }

    @Override
    public String getSimpleBindStatement() {
        return mSimpleBindStatement;
    }

    @Override
    public String getSimpleUnbindStatement() {
        return mSimpleUnbindStatement;
    }

    @Override
    public String getCanonicalUnbindStatement() {
        return getSimpleUnbindStatement();
    }

    @Override
    public String getUnbinderClassSimpleName() {
        return mUnbinderClassSimpleName;
    }
}

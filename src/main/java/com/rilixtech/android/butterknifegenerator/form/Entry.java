package com.rilixtech.android.butterknifegenerator.form;

import com.rilixtech.android.butterknifegenerator.iface.OnCheckBoxStateChangedListener;
import com.rilixtech.android.butterknifegenerator.model.Element;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

class Entry extends JPanel {
    private Element mElement;
    private OnCheckBoxStateChangedListener mListener;

    private JCheckBox mCheck;
    private JLabel mType;
    private JLabel mID;
    private JCheckBox mEvent;
    private JCheckBox mCbxOnTextChanged;
    private JTextField mName;
    private Color mNameDefaultColor;
    private Color mNameErrorColor = new Color(0x880000);

    JCheckBox getCheck() {
        return mCheck;
    }

    void setListener(final OnCheckBoxStateChangedListener onStateChangedListener) {
        this.mListener = onStateChangedListener;
    }

    Entry(Element element, ArrayList<String> generatedIDs) {
        mElement = element;

        mCheck = new JCheckBox();
        mCheck.setPreferredSize(new Dimension(30, 26));
        if (generatedIDs.contains(element.getFullID()) || generatedIDs.contains(element.getFullIDOfLibrary())) {
            mCheck.setSelected(false);
        } else {
            mCheck.setSelected(mElement.used);
        }
        mCheck.addChangeListener(new CheckListener());

        mEvent = new JCheckBox();
        mEvent.setPreferredSize(new Dimension(60, 26));

        mCbxOnTextChanged = new JCheckBox();
        mCbxOnTextChanged.setPreferredSize(new Dimension(120, 26));

        mType = new JLabel(mElement.name);
        mType.setPreferredSize(new Dimension(100, 26));

        mID = new JLabel(mElement.id);
        mID.setPreferredSize(new Dimension(100, 26));

        mName = new JTextField(mElement.fieldName, 10);
        mNameDefaultColor = mName.getBackground();
        mName.setPreferredSize(new Dimension(100, 26));
        mName.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // empty
            }

            @Override
            public void focusLost(FocusEvent e) {
                syncElement();
            }
        });

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        setMaximumSize(new Dimension(Short.MAX_VALUE, 54));
        add(mCheck);
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(mType);
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(mID);
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(mEvent);
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(mCbxOnTextChanged);
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(mName);
        add(Box.createHorizontalGlue());

        checkState();
    }

    void syncElement() {
        mElement.used = mCheck.isSelected();
        mElement.isClick = mEvent.isSelected();
        mElement.fieldName = mName.getText();
        mElement.setOnTextChanged(mCbxOnTextChanged.isSelected());

        if (mElement.checkValidity()) {
            mName.setBackground(mNameDefaultColor);
        } else {
            mName.setBackground(mNameErrorColor);
        }
    }

    private void checkState() {
        mType.setEnabled(mCheck.isSelected());
        mID.setEnabled(mCheck.isSelected());
        mName.setEnabled(mCheck.isSelected());
        if (mListener != null) mListener.changeState(mCheck.isSelected());
    }

    public class CheckListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent event) {
            checkState();
        }
    }

}

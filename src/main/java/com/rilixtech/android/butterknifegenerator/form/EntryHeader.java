package com.rilixtech.android.butterknifegenerator.form;

import com.rilixtech.android.butterknifegenerator.iface.OnCheckBoxStateChangedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class EntryHeader extends JPanel {

    private JCheckBox mAllCheck;
    private OnCheckBoxStateChangedListener mAllListener;

    void setAllListener(final OnCheckBoxStateChangedListener onStateChangedListener) {
        this.mAllListener = onStateChangedListener;
    }

    EntryHeader() {
        mAllCheck = new JCheckBox();
        mAllCheck.setPreferredSize(new Dimension(30, 26));
        mAllCheck.setSelected(false);
        mAllCheck.addItemListener(new AllCheckListener(mAllListener));

        JLabel lblType = new JLabel("Element");
        lblType.setPreferredSize(new Dimension(100, 26));
        lblType.setFont(new Font(lblType.getFont().getFontName(), Font.BOLD, lblType.getFont().getSize()));

        JLabel lblID = new JLabel("ID");
        lblID.setPreferredSize(new Dimension(90, 26));
        lblID.setFont(new Font(lblID.getFont().getFontName(), Font.BOLD, lblID.getFont().getSize()));

        JLabel lblOnClick = new JLabel("OnClick");
        lblOnClick.setPreferredSize(new Dimension(60, 26));
        lblOnClick.setFont(new Font(lblOnClick.getFont().getFontName(), Font.BOLD, lblOnClick.getFont().getSize()));

        JLabel lblOnTextChanged = new JLabel("OnTextChanged");
        lblOnTextChanged.setPreferredSize(new Dimension(120, 26));
        lblOnTextChanged.setFont(new Font(lblOnTextChanged.getFont().getFontName(), Font.BOLD, lblOnTextChanged.getFont().getSize()));

        JLabel lblName = new JLabel("Variable Name");
        lblName.setPreferredSize(new Dimension(120, 26));
        lblName.setFont(new Font(lblName.getFont().getFontName(), Font.BOLD, lblName.getFont().getSize()));

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        add(Box.createRigidArea(new Dimension(1, 0)));
        add(mAllCheck);
        add(Box.createRigidArea(new Dimension(11, 0)));
        add(lblType);
        add(Box.createRigidArea(new Dimension(12, 0)));
        add(lblID);
        add(Box.createRigidArea(new Dimension(12, 0)));
        add(lblOnClick);
        add(Box.createRigidArea(new Dimension(13, 0)));
        add(lblOnTextChanged);
        add(Box.createRigidArea(new Dimension(22, 0)));
        add(lblName);
        add(Box.createHorizontalGlue());
    }

    JCheckBox getAllCheck() {
        return mAllCheck;
    }

    private class AllCheckListener implements ItemListener {
        private OnCheckBoxStateChangedListener listener;

        AllCheckListener(OnCheckBoxStateChangedListener listener) {
            this.listener = listener;
        }
        @Override
        public void itemStateChanged(ItemEvent itemEvent) {
            if (listener != null) {
                listener.changeState(itemEvent.getStateChange() == ItemEvent.SELECTED);
            }
        }
    }
}

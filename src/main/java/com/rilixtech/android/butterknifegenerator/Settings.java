package com.rilixtech.android.butterknifegenerator;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.rilixtech.android.butterknifegenerator.common.Utils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Settings UI for the plugin.
 *
 * @author David Vávra (vavra@avast.com)
 */
public class Settings implements Configurable {

    public static final String PREFIX = "butterknifegenerator_prefix";
    public static final String USE_LAST_PART = "butterknifegenerator_use_last_part";
    public static final String VIEWHOLDER_CLASS_NAME = "butterknifegenerator_viewholder_class_name";

    private JPanel mPanel;
    private JTextField mHolderName;
    private JTextField mPrefix;
    private JCheckBox mCbxUseLastPart;

    @Nls
    @Override
    public String getDisplayName() {
        return "ButterKnifeGenerator";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        reset();
        return mPanel;
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() throws ConfigurationException {
        PropertiesComponent.getInstance().setValue(PREFIX, mPrefix.getText());
        PropertiesComponent.getInstance().setValue(VIEWHOLDER_CLASS_NAME, mHolderName.getText());
        PropertiesComponent.getInstance().setValue(USE_LAST_PART, mCbxUseLastPart.isSelected());
    }

    @Override
    public void reset() {
        mPrefix.setText(Utils.getPrefix());
        mHolderName.setText(Utils.getViewHolderClassName());
        mCbxUseLastPart.setSelected(Utils.UseLastPartForViewPrefix());
    }

    @Override
    public void disposeUIResources() {

    }
}

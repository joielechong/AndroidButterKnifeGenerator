package com.rilixtech.android.butterknifegenerator.model;


import com.rilixtech.android.butterknifegenerator.common.Utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Element {

    private static final Pattern sIdPattern = Pattern.compile("@\\+?(android:)?id/([^$]+)$", Pattern.CASE_INSENSITIVE);
    private static final Pattern sValidityPattern = Pattern.compile("^([a-zA-Z_\\$][\\w\\$]*)$", Pattern.CASE_INSENSITIVE);

    public String id;
    public boolean isAndroidNS = false;
    public String nameFull; // element name with package
    public String name; // element name
    public String fieldName; // name of variable
    public boolean used = true;
    public boolean isClick = true;
    private boolean isOnTextChanged = true;

    public Element(String name, String id) {
        // id
        final Matcher matcher = sIdPattern.matcher(id);
        if (matcher.find() && matcher.groupCount() > 0) {
            this.id = matcher.group(2);

            String androidNS = matcher.group(1);
            this.isAndroidNS = !(androidNS == null || androidNS.length() == 0);
        }

        // name
        String[] packages = name.split("\\.");
        if (packages.length > 1) {
            this.nameFull = name;
            this.name = packages[packages.length - 1];
        } else {
            this.nameFull = null;
            this.name = name;
        }

        this.fieldName = getFieldName();
    }

    public boolean isOnTextChanged() {
        return isOnTextChanged;
    }

    public void setOnTextChanged(boolean onTextChanged) {
        isOnTextChanged = onTextChanged;
    }

    /**
     * Create full ID for using in layout XML Files
     * @param isModuleLibrary is layout inside application or library module
     * @return id of view.
     */
    public String getFullID(boolean isModuleLibrary) {
        if(isModuleLibrary) {
            return getFullIDOfLibrary();
        } else {
            return getFullID();
        }
    }

    /**
     * Create full ID for using in layout XML files
     *
     * @return id for
     */
    public String getFullID() {
        StringBuilder fullID = new StringBuilder();
        String rPrefix;

        if (isAndroidNS) {
            rPrefix = "android.R.id.";
        } else {
            rPrefix = "R.id.";
        }

        fullID.append(rPrefix);
        fullID.append(id);

        return fullID.toString();
    }

    public String getFullIDOfLibrary() {
        StringBuilder fullID = new StringBuilder();
        String rPrefix;

        if (isAndroidNS) {
            rPrefix = "android.R.id.";
        } else {
            rPrefix = "R2.id.";
        }

        fullID.append(rPrefix);
        fullID.append(id);

        return fullID.toString();
    }

    /**
     * Generate field name if it's not done yet
     *
     * @return field name
     */
    private String getFieldName() {
        String[] words = this.id.split("_");
        StringBuilder sb = new StringBuilder();

        boolean withPrefix = !Utils.getPrefix().isEmpty();
        if(withPrefix) sb.append(Utils.getPrefix());

        int wordsLength = words.length;
        if (Utils.UseLastPartForViewPrefix()) {
            if (wordsLength >= 2) {
                // Append last chars as view id
                int lastChars = wordsLength - 1;
                sb.append(getCharsWithUppercasedFirstChar(lastChars, words[lastChars]));

                for (int i = 0; i < wordsLength - 1; i++) {
                    if(!withPrefix && i == 0) {
                        sb.append(words[i]);
                    } else {
                        sb.append(getCharsWithUppercasedFirstChar(i, words[i]));
                    }
                }

            } else {
                if(withPrefix) {
                    sb.append(getCharsWithUppercasedFirstChar(1, words[0]));
                } else {
                    sb.append(words[0]);
                }
            }
        } else {
            //  Don't uppercase first character of the first string when there is no prefix
            for (int i = 0; i < wordsLength; i++) {
                if(!withPrefix && i == 0) {
                    sb.append(words[i]);
                } else {
                    sb.append(getCharsWithUppercasedFirstChar(i, words[i]));
                }
            }
        }

        return sb.toString();
    }

    private char[] getCharsWithUppercasedFirstChar(int position, String text) {
        String[] tokens = text.split("\\.");
        char[] tokenChars = tokens[tokens.length - 1].toCharArray();
        if (position > 0 || !Utils.isEmptyString(Utils.getPrefix())) {
            tokenChars[0] = Character.toUpperCase(tokenChars[0]);
        }
        return tokenChars;
    }

    /**
     * Check validity of field name
     *
     * @return validity of pattern
     */
    public boolean checkValidity() {
        Matcher matcher = sValidityPattern.matcher(fieldName);
        return matcher.find();
    }
}
